//
//  Wage.swift
//  window-shopper
//
//  Created by Jenia on 5/28/18.
//  Copyright © 2018 Jenia. All rights reserved.
//

import Foundation
class Wage {
    class func getHours(forWage wage: Double, andPrice price: Double) -> Int {
        return Int(ceil(price/wage) )
    }
}
